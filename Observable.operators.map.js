Observable.prototype.map = function(mapFn) {
  const input = this;
  return new Observable((observer) => {
    return input.subscribe(OperatorObserver(mapFn));
  });
};



