// --------------------------
// APPLICATION | simple observer:
let one$ = new Observable((observer) => {
  observer.next(1);
  observer.next(2);
});
one$.subscribe(SimpleObserver());


// --------------------------
// APLICATION | fromEvent - static:
let el = document.querySelector('#observe_node');
let node$ = Observable.fromEvent(el, 'keyup');
let unsubscribe = node$.subscribe(SimpleObserver());





