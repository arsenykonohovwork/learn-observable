// --------------------------
// LIBRARY:
Observable.fromEvent = (elem, name) => {
  return new Observable((observer) => {
    const handler = (e) => observer.next(e.target.value);
    elem.addEventListener('keyup', handler, false);
    return () => elem.removeEventListener(name, handler, false);
  });
}
