const SimpleObserver = () => ({
  next(item) {console.log(item)},
  error(err) {console.log(err)},
  complete() {console.log('COMPLETED')}
});

const OperatorObserver = (operator) => ({
  next(item) {console.log(operator(item))},
  error(err) {console.log(err)},
  complete() {console.log('COMPLETED')}
});








